# FooBarQix <!-- omit in toc -->

## Sommaire <!-- omit in toc -->
- [Principe](#principe)
- [Initialisation](#initialisation)
- [A. Entier vers string](#a-entier-vers-string)
- [B. Divisible par 3](#b-divisible-par-3)
- [C. Divisible par 5](#c-divisible-par-5)
- [D. Divisible par 7](#d-divisible-par-7)
- [E. Divisible par au moins 2 nombres parmi 3, 5 et 7](#e-divisible-par-au-moins-2-nombres-parmi-3-5-et-7)
- [F. Contient 3](#f-contient-3)
- [G. Contient 5](#g-contient-5)
- [H. Contient 7](#h-contient-7)
- [I. Contient deux chiffres parmi 3, 5 ou 7](#i-contient-deux-chiffres-parmi-3-5-ou-7)
- [J. Garder une trace des zéros](#j-garder-une-trace-des-zéros)

## Principe

Mettez vous en binôme, vous allez faire du TDD en mode **ping-pong** !

Pour rappel le code en mode ping-pong fonctionne sur le principe suivant :
1. binôme A écrit le test
2. binôme B écrit l'implémentation
3. binôme A restructure le code
4. binôme B écrit le test suivant
5. binôme A écrit l'implémentation
6. etc.

Vous allez implémenter la classe suivante :

``` java
public class FooBarQix {
    public static String compute (Integer entry) {
        ...
    }
}
```

## Initialisation

### configuration de maven

  - s'il n'existe pas déjà créez un dossier ".m2" : `mkdir $HOME/.m2`
  - création du fichier de configuration : `touch $HOME/.m2/settings.xml`
  - avec votre éditeur de texte préféré, mettez ce qu'il y a ci-dessous dans `settings.xml`
  - finalement, vérifiez votre installation : `mvn -version`

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <proxies>
      <proxy>
        <id>proxy</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>cache.univ-lille1.fr</host>
        <port>3128</port>
      </proxy>
    </proxies>
</settings>
```

### Projet

* Demandez à rejoindre le groupe gitlab suivant : https://gitlab.univ-lille.fr/2019-coa-tdd/groupe-[Votre groupe]/
* Créez un projet dans ce groupe, nommé `tp0-nom1-nom2`
* Dans un terminal, créez un workspace pour la coa-tdd `mkdir ~/ws-coa-tdd`
* Clonez le squelette de projet maven `git clone https://gitlab.univ-lille.fr/2019-coa-tdd/maven-skeleton.git ~/ws-coa-tdd/tp0-nom1-nom2`
* Dans un terminal, entrez dans le répertoire `~/ws-coa-tdd/tp0-nom1-nom2`
* Changer l'URL du projet gitlab pour y mettre la votre `git remote set-url origin URL`
* Modifier le fichier `pom.xml` changez le nom du projet dans le fichier pom.xml `sed -i -e 's/maven-skeleton/tp0-nom1-nom2/g' pom.xml`
* Lancez les tests pour vérifier que tout fonctionne `mvn test`
* Ouvrez éclipse `eclipse -data ~/ws-coa-tdd/`
* Importez votre nouveau projet maven à l'aide du menu File > Import : ![](import.png)

Maintenant vous avez tout ce qu'il faut pour commencer à travailler.

## A. Entier vers string

Avec un entier en entrée, la méthode `FooBarQix.compute()` va nous retourner la version `String` par exemple :

| Entrée | Sortie |
|--------|--------|
| 1      | "1"    |
| 31     | "31"   |

### A.1. Codez le premier test

À l'image de la classe ExampleTest, commencez par créer une nouvelle classe FooBarQixTest avec une première méthode de test. Par exemple :

``` java
public class FooBarQixTest {
    @Test
    public void should_return_input_as_string () {
        Assert.assertEquals("1", FooBarQix.compute(1));
    }
}
```

Pour pouvoir lancer le test (et l'observer échouer en rouge), il faut créer une nouvelle classe FooBarQix avec la méthode compute.

``` java
public class FooBarQix {
    public static String compute (Integer entry) {
        return "";
    }
}
```

Lancez maintenant le test :
- soit avec la commande `mvn test` :
- soit dans éclipse avec "clic droit" > "Run as" > "JUnit test"
- soit dans IntelliJ avec <kbd>CTRL</kbd>+<kbd>SHIFT</kbd>+<kbd>F10</kbd>

Vous verrez que le test échoue puisque la méthode FooBarQix::compute retourne une chaîne vide alors que le test s'attend à recevoir la chaîne `"1"`.

### A.2. Codez la première implémentation

L'idée c'est de faire passer le test au vert le plus simplement possible. Par exemple avec le code suivant :

``` java
public class FooBarQix {
    public static String compute (Integer entry) {
        return "1";
    }
}
```

Vérifiez que le test passe correctement. Si tout se passe bien (tests au vert) commitez votre travail.

### A.3. Restructurez le code

Restructurez le code de façon à obtenir un code dont vous êtes fier, avec des méthodes de test explicites, capables de bien exprimer le besoin métier.

Une fois que vous êtes satisfaits de votre travail, commitez et pushez votre code.

### A.4. Itérez

Recommencez la boucle TDD tant que la règle métier n'est pas correctement implémentée : bien qu'on ait un test vert, en réalité on n'a pas encore vraiment implémenté la règle métier.

Il faut donc un 2e tour de boucle dans lequel vous allez :
- écrire un nouveau test (dans la même méthode) qui permette de vérifier la règle métier
- recoder la méthode compute afin de la rendre générique
- restructurer le code

N'oubliez pas les commit/push à chaque étape de cette nouvelle boucle.

## B. Divisible par 3

Si l'entrée est divisible par 3 alors la méthode `FooBarQix.compute()` doit maintenant retourner "Foo".

Exemples :

| Entrée | Sortie |
|--------|--------|
| 6      | "Foo"  |
| 78     | "Foo"  |

Créez une nouvelle méthode de test dans la classe `FooBarQixTest.java` pour tester cette nouvelle fonctionnalité. Réalisez la boucle TDD autant de fois que nécessaire.

Chaque fois que vous abordez la phase de restructuration, posez vous les questions suivantes :
- Sommes-nous fiers de notre code ?
- Avons-nous bien appliqué tous les principes SOLID ?
- Pouvons-nous appliquer un design pattern pour voir s'il serait adapté ?

## C. Divisible par 5

Si l'entrée est divisible par 5 alors la méthode `FooBarQix.compute()` nous retourne "Bar"

Exemples :

| Entrée | Sortie |
|--------|--------|
| 10     | "Bar"  |
| 130    | "Bar"  |

Réalisez la boucle TDD autant de fois que nécessaire.

## D. Divisible par 7

Si l'entrée est divisible par 7 alors la méthode `FooBarQix.compute()` nous retourne "Qix"

Exemples :

| Entrée | Sortie |
|--------|--------|
| 14     | "Qix"  |
| 182    | "Qix"  |

Réalisez la boucle TDD autant de fois que nécessaire.

## E. Divisible par au moins 2 nombres parmi 3, 5 et 7

Si l'entrée est divisible par au moins 2 des nombres 3, 5 ou 7 alors la méthode `FooBarQix.compute()` nous retourne une composition de "Foo", "Bar" ou "Qix" respectant l'ordre "Foo", "Bar", "Qix"

Exemples :

| Entrée | Sortie      |
|--------|-------------|
| 30     | "FooBar"    |
| 70     | "BarQix"    |
| 21     | "FooQix"    |
| 210    | "FooBarQix" |

Réalisez la boucle TDD temps que tous les cas fonctionnels ne sont pas testés.

## F. Contient 3

Si l'entrée contient le chiffre 3 alors la méthode `FooBarQix.compute()` remplace chaque occurrence par "Foo" et supprime les chiffres non remplacés.

Exemples :

| Entrée | Sortie |Commentaire |
|--------|--------|------------|
| 13     | "Foo"  | |
| 3113   | "FooFoo"  ||
| 3      | "FooFoo" | Contient "3" et est divisible par 3|

Réalisez la boucle TDD autant de fois que nécessaire.

## G. Contient 5

Si l'entrée contient le chiffre 5 alors la méthode `FooBarQix.compute()` remplace chaque occurrence par "Bar" et supprime les chiffres non remplacés.

Exemples :

| Entrée | Sortie |
|--------|--------|
| 5111     | "Bar"  |
| 551    | "BarBar"  |

Réalisez la boucle TDD autant de fois que nécessaire.

## H. Contient 7

Si l'entrée contient le chiffre 7 alors la méthode `FooBarQix.compute()` remplace chaque occurrence par "Qix" et supprime les chiffres non remplacés.

Exemples :

| Entrée | Sortie |
|--------|--------|
| 71     | "Qix"    |
| 7724   | "QixQix" |

Réalisez la boucle TDD autant de fois que nécessaire.


## I. Contient deux chiffres parmi 3, 5 ou 7

Si l'entrée contient au moins 2 chiffres parmi 3, 5 ou 7 alors la méthode `FooBarQix.compute()` remplace chaque occurrence de 3, 5 ou 7 par "Foo", "Bar" ou "Qix" en respectant l'ordre et supprime les chiffres non remplacés.

Exemples :

| Entrée | Sortie |
|--------|--------|
| 173    | "QixFoo" |

Réalisez la boucle TDD autant de fois que nécessaire.

## J. Garder une trace des zéros

Si l'entrée contient des "0", les remplacez par des "*"

Exemples :

| Entrée  | Sortie   |
|---------|----------|
| 1703    | "Qix*Foo" |

Réalisez la boucle TDD autant de fois que nécessaire.

## Une solution <!-- omit in toc -->

https://tube.tux.ovh/videos/watch/809a8c8f-1e56-4336-9683-593efbf24bec
